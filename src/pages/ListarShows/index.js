import * as React from 'react';
import { View, Text, StyleSheet, StatusBar, Alert,ScrollView, FlatList, Button, Switch,NativeEventEmitter, TouchableOpacity,
  RefreshControl } from 'react-native';


import Realm from 'realm';
let realm;

const ShowSchema = {
  name: 'Show',
  properties: {
    artista:  'string',
    valor: {type: 'int', default: 0},
    genero: 'string',
    principal: {type: 'bool', default: false},
    hora_inicio: 'string',
    hora_fim: 'string',
    data: 'string',
  }
};

export default class listar extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      showpricipal: false,
      isLoading: false,
      Shows: [],
    };
    Realm.open({schema: [ShowSchema]});
    realm = new Realm();
    var shows_items = realm.objects('Show');
    this.state = {
      showpricipal: false,
      isLoading: false,
      Shows: shows_items,
    };
    this.getShowPrincipal()
  }
  componentWillMount() {
    this.getShowPrincipal()
    setInterval(() => {
      Realm.open({schema: [ShowSchema]}).then(realm => {
        this.setState({ Shows: realm.objects('Show') });
      });
    }, 2000);
  }
  getShowPrincipal(){
     new Promise((resolve, reject) => {
      Realm.open({ schema: [ShowSchema] })
        .then(realm => {
          var query = 'data == "' + this.dataAtualFormatada() + '" and principal == true'
          var collectionListenerRetainer = realm.objects('Show').filtered(query);
          resolve(collectionListenerRetainer)
        })
        .catch(error => {
          reject(error);
        });
    }).then((r)=>{
      
      if(r.length != 0){
        console.log("r", r)
        this.setState({ showpricipal: r[0] });
      }
    })
  }
  dataAtualFormatada(){
    var data = new Date(),
        dia  = data.getDate().toString(),
        diaF = (dia.length == 1) ? '0'+dia : dia,
        mes  = (data.getMonth()+1).toString(),
        mesF = (mes.length == 1) ? '0'+mes : mes,
        anoF = data.getFullYear();
    return diaF+"/"+mesF+"/"+anoF;
  }
  renderRefreshControl(){
    this.getShowPrincipal()
    Realm.open({schema: [ShowSchema]}).then(realm => {
      this.setState({ Shows: realm.objects('Show') });
    });
  }

  ListViewItemSeparator = () => {
    return (
      <View style={{ height: 0.5, width: '100%', backgroundColor: '#000' }} />
    );
  };
  deletar(item){
    var self = this
    Alert.alert(
      'Deletar',
      'Você deseja realmente deletar o show de '+item.artista+'?',
      [
        {
          text: 'Não',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Sim', onPress: () => {
          Realm.open({schema: [ShowSchema]}).then(realm => {
            
            realm.write(() => {
              realm.delete(item);
            })
            self.renderRefreshControl()
          });
         
        }},
      ],
      {cancelable: false},
    );
  }
  render() {
    return (
      <View  style={{ backgroundColor: '#ededed' }}>
      {this.state.showpricipal ? () => {
        console.log("soos",this.state.showpricipal)
        return <View><Text>Show Principal</Text><Text>{this.state.showpricipal.artista}</Text></View>;
      } : ()=>{
        console.log("soos",this.state.showpricipal)
        return <View><Text>Não existem show principal para a data de hoje</Text></View>;
      }}
      <FlatList
        data={this.state.Shows}
        onRefresh={() => this.renderRefreshControl()}
        refreshing={this.state.isLoading}
        ItemSeparatorComponent={this.ListViewItemSeparator}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={()=>{this.deletar(item)}}>
          <View style={{ backgroundColor: '#ededed', padding: 20 }}>
            <Text>Artista/Banda: {item.artista}</Text>
            <Text>Valor: R$ {item.valor}</Text>
            <Text>Gênero: {item.genero}</Text>
            <Text>Show principal: {item.principal ? 'Sim' : 'Não'}</Text>
            <Text>Hora de início: {item.hora_inicio}</Text>
            <Text>Hora do fim: {item.hora_fim}</Text>
            <Text>Data: {item.data}</Text>
          </View>
          </TouchableOpacity>
        )}
      />
    </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
  },
  scene: {
    flex: 1,
  },
});

import * as React from 'react';
import { View, Text, StyleSheet, StatusBar, ScrollView, TextInput, Button, Switch } from 'react-native';
import DatePicker from 'react-native-datepicker';
import TextInputMask from 'react-native-text-input-mask';
const Realm = require('realm');
const ShowSchema = {
  name: 'Show',
  properties: {
    artista: 'string',
    valor: { type: 'int', default: 0 },
    genero: 'string',
    principal: { type: 'bool', default: false },
    hora_inicio: 'string',
    hora_fim: 'string',
    data: 'string',
  }
};

export default class cadastro extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      artista: '',
      valor: 0.0,
      genero: '',
      principal: false,
      hora_inicio: '',
      hora_fim: '',
      data: '',
    }
  }
  limpacampos() {
    this.setState({
      artista: '',
      valor: 0.0,
      genero: '',
      principal: false,
      hora_inicio: '',
      hora_fim: '',
      data: '',
    })
  }

  verificaPrincipal(data) {
    return new Promise((resolve, reject) => {
      Realm.open({ schema: [ShowSchema] })
        .then(realm => {
          var query = 'data == "' + data + '" and principal == true'
          var collectionListenerRetainer = realm.objects('Show').filtered(query);
          var result = (collectionListenerRetainer.length == 0) ? false : true;
          resolve(result)
        })
        .catch(error => {
          reject(error);
        });
    })
  }
verificaNulos(){
  if(!this.state.artista){
    alert("Preencha o campo Artista/Banda")
    return false;
  }
  if(!this.state.valor){
    alert("Preencha o campo Valor")
    return false;
  }
  if(!this.state.genero){
    alert("Preencha o campo Gênero")
    return false;
  }
  if(!this.state.hora_fim){
    alert("Preencha o campo Hora Final")
    return false;
  }
  if(!this.state.hora_inicio){
    alert("Preencha o campo Hora de Início")
    return false;
  }
  return true;
}
  salvar() {
    var show = this.state;
    this.verificaPrincipal(show.data).then(existePrincipal => {
      if (existePrincipal && show.principal) {
        alert("Já existe um show principal nesta data")
      } else {
        if (this.verificaNulos()){
          Realm.open({ schema: [ShowSchema] })
            .then(realm => {
              realm.write(() => {
                const myShow = realm.create('Show', {
                  artista: show.artista,
                  valor: show.valor,
                  genero: show.genero,
                  principal: show.principal,
                  hora_inicio: show.hora_inicio,
                  hora_fim: show.hora_fim,
                  data: show.data
                });
                this.limpacampos();
                alert("Salvo com sucesso!")
              });
              realm.close();
            })
            .catch(error => {
              console.log(error);
            });
          }
      }
    })

  }

  render() {
    return (
      <View>
        <ScrollView>
          <View style={{ padding: 20 }}>
            <Text>Artista/Banda:</Text>
            <TextInput style={{ borderBottomWidth: 1 }} onChangeText={(text) => { this.setState({ artista: text }) }} />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Valor:</Text>
            <TextInput keyboardType='numeric' style={{ borderBottomWidth: 1 }} onChangeText={(text) => { this.setState({ valor: Number(text) }) }} />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Gênero:</Text>
            <TextInput style={{ borderBottomWidth: 1 }} onChangeText={(text) => { this.setState({ genero: text }) }} />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Show Principal:</Text>
            <Switch
              style={{ marginTop: 30 }}
              onValueChange={ (text) => { this.setState({ principal: text }) }}
              value={this.state.principal} />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Hora de Início:</Text>
            <TextInputMask
              refInput={ref => { this.input = ref }}
              onChangeText={(formatted, extracted) => {
                console.log(formatted) 
                console.log(extracted) 
              }}
              keyboardType='numeric' style={{ borderBottomWidth: 1 }} onChangeText={(text) => { this.setState({ hora_inicio: text }) }}
              mask={"[00]:[00]"}
            />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Hora Final:</Text>
            <TextInputMask
              refInput={ref => { this.input = ref }}
              onChangeText={(formatted, extracted) => {
                console.log(formatted) 
                console.log(extracted) 
              }}
              keyboardType='numeric' style={{ borderBottomWidth: 1 }} onChangeText={(text) => { this.setState({ hora_fim: text }) }}
              mask={"[00]:[00]"}
            />
          </View>
          <View style={{ padding: 20 }}>
            <Text>Data</Text>
            <DatePicker
              style={{ width: 300 }}
              date={this.state.data}
              mode="date"
              placeholder="Selecione uma data"
              format="DD/MM/YYYY"
              minDate="01-01-2020"
              confirmBtnText="Confirmar"
              cancelBtnText="Cancelar"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 7,
                  marginLeft: 0
                },
                dateInput: {
                  left: 3,
                  top: 5,
                  marginLeft: 36
                }
              }}
              onDateChange={(date) => {
                this.setState({ data: date })
              }}
            />
          </View>
          <View style={{ padding: 20 }}>
            <Button
              title="Salvar"
              onPress={() => this.salvar()}
            />
          </View>
        </ScrollView>

      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
  },
  scene: {
    flex: 1,
  },
});

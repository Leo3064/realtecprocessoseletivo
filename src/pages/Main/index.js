import * as React from 'react';
import { View, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
);
import cadastro from '../CadastroShow';
import listar from '../ListarShows';

const initialLayout = { width: Dimensions.get('window').width };

export default function TabViewExample() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Listar Shows' },
    { key: 'second', title: 'Cadastrar' },
  ]);

  const renderScene = SceneMap({
    first: listar,
    second: cadastro,
  });

  return (
    <View  style={{ backgroundColor: '#ededed',flex:1 }}>
      
      <TabView
        style={{ backgroundColor: '#ededed' }}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        style={styles.container}
      />
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
  },
  scene: {
    flex: 1,
  },
});

## Processo seletivo Realtec


## Sobre o Projeto

Este projeto foi construído como apresentação para o processo seletivo da empresa Realtec para o cargo de Programador Jr. 
Sua função é cadastrar shows de carnaval para a prefeitura com um show principal por dia, não podendo existir mais de um show principal para o mesmo dia. Consta com as variáveis de armazenamento de dados de: nome do artista ou banda, gênero da atração, o valor pago pelo show, a hora de início e sua hora final. 
Foi desenvolvido na linguagem JavaScript com o complemento da tecnologia Node.js para um melhor desempenho e armazenamento de dados. 

## Dependências

Node js versão 10.16.0

Android Studio

## Inicialização do Projeto

Baixar o arquivo ZIP da plataforma GitLab;

Renomear, se necessário, a pasta extraída para `realtecprocessoseletivo`;

Abrir o programa Visual Studio Code; 

Abrir o terminal;

Selecionar para abrir a pasta extraída `realtecprocessoseletivo`;

Instalar as dependências necessárias ao projeto com o comando `npm install`;

Conectar um aparelho SmartPhone que possua o S.O Android ao computador; 

Abrir as configurações e ativar a depuração USB no celular;

Executar o projeto para Android com o comando `npm run android`.

## Links Úteis

https://reactnative.dev/docs/getting-started.html
